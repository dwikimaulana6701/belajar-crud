@extends('layout.master')
@section('judul')
    Edit Cast {{$pemeran->nama}}
@endsection

@section('isi')

<form action="/cast/{{$pemeran->id}}" method="POST">
  @csrf
  @method('put')
    <div class="form-group">
      <label>Nama Cast</label>
      <input type="string" value="{{$pemeran->nama}}" name="nama" class="form-control">
    </div>
    @error('nama')
    <div class="alert alert-danger">{{ $message }}</div>
    @enderror
    <div class="form-group">
      <label>Umur Cast</label>
      <input type="string" name="umur" class="form-control">
    </div>
    @error('umur')
    <div class="alert alert-danger">{{ $message }}</div>
    @enderror
    <div class="form-group">
      <label>Bio Cast</label>
      <textarea name="bio" cols="30" rows="10" class="form-control">{{$pemeran->bio}}</textarea>
    </div>
    @error('bio')
    <div class="alert alert-danger">{{ $message }}</div>
    @enderror
    <button type="submit" class="btn btn-primary">Update</button>
  </form>

@endsection