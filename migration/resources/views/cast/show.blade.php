@extends('layout.master')
@section('judul')
    Detail Cast {{$pemeran->nama}}
@endsection

@section('isi')

<h1>{{$pemeran->nama}}</h1>
<p>{{$pemeran->umur}}</p>
<p>{{$pemeran->bio}}</p>

<a href="/cast" class="btn btn-secondary">Kembali</a>

@endsection