@extends('layout.master')
@section('judul')
    Halaman Pendaftaran
@endsection

@section('isi')
<h1>Buat Account Baru!</h1>
    <h3>Sign Up Form</h3>
    <form action="/welcome" method="post">
        @csrf
        <label>First Name:</label><br><br>
        <input type="text" name="fname" required><br><br>
        <label>Last Name:</label><br><br>
        <input type="text" name="lname"required><br><br>

        <label>Gender:</label><br><br>
        <input type="radio" name="jenkel" required>Male 
        <input type="radio" name="jenkel" required>Female
        <input type="radio" name="jenkel" required>Order<br><br>

        <label>Nationality</label><br><br>
        <select name="negara" required>
            <option value="1"> Indonesia </option>
            <option value="2"> Singapura </option>
            <option value="3"> Malaysia </option>
        </select><br><br>

        <label>Language Spoken</label><br><br>
        <input type="checkbox"> Bahasa Indonesia
        <input type="checkbox"> Inggris
        <input type="checkbox"> Order<br><br>
        
        <label>Bio:</label><br><br>
        <textarea name="motivasi" cols="40" rows="10" required></textarea><br>
        <br>
        <button type="submit">Sign Up</button>
    </form>
@endsection